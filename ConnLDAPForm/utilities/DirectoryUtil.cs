﻿using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using ConnLDAPForm.libraries;

namespace ConnLDAPForm.utilities
{
    class DirectoryUtil
    {
        public static string _ldapConnectionString;


        public static string LdapConnectionString
        {
            get
            {
                return _ldapConnectionString;
            }
            set
            {
                _ldapConnectionString = value;
            }
        }

        public static void getConnectionString()
        {
            if(ConfigVar.SecureLDAP && ConfigVar.LdapServerPort != "")
            {
                LdapConnectionString = ConfigVar.LdapServerHost + ":" + ConfigVar.LdapServerPort;
            }
            else
            {
                LdapConnectionString = ConfigVar.LdapServerHost;
            }
        }


        public static string GetMachineUsername()
        {
            String FullUsername = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String[] SplittedUn;
            SplittedUn = FullUsername.Split('\\');
            return SplittedUn[1];
        }

        public static bool CheckUserCredentials()
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, ConfigVar.LdapServerHost);
            using (ctx)
            {
                // validate the credentials
                return ctx.ValidateCredentials(ConfigVar.LdapUser, ConfigVar.LdapPassword);
            }
        }

        public static void GetUserGroupOu(string username, string groupname)
        {

            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, ConfigVar.LdapServerHost);

            UserPrincipal usr = UserPrincipal.FindByIdentity(ctx,
                                                       IdentityType.SamAccountName,
                                                       username);

            GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, groupname);


            if (usr != null)
            {
                if (group != null)
                {
                    if (usr.IsMemberOf(group))
                    {
                        Console.WriteLine("User is part of the group: " + groupname);
                        string result = string.Empty;
                        // Getting the DirectoryEntry
                        DirectoryEntry directoryEntry = (usr.GetUnderlyingObject() as DirectoryEntry);
                        //if the directoryEntry is not null
                        if (directoryEntry != null)
                        {
                            //Getting the directoryEntry's path and spliting with the "," character
                            string[] directoryEntryPath = directoryEntry.Path.Split(',');
                            //Getting the each items of the array and spliting again with the "=" character
                            foreach (var splitedPath in directoryEntryPath)
                            {
                                string[] eleiments = splitedPath.Split('=');
                                //If the 1st element of the array is "OU" string then get the 2dn element
                                if (eleiments[0].Trim() == "OU")
                                {
                                    result = username + "-" + eleiments[1].Trim();
                                    Console.WriteLine("User is part of the OU: " + result);
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("User is NOT part of the group: " + groupname);
                    }
                }
                else
                {
                    Console.WriteLine("Group: " + group + " not found");
                }
            }
            else
            {
                Console.WriteLine("User is Null");
            }
            ctx.Dispose();
        }


        public static void GetUser(string username)
        {
            Console.WriteLine("Ldap Server: " + ConfigVar.LdapServerHost);

            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, ConfigVar.LdapServerHost);

            UserPrincipal usr = UserPrincipal.FindByIdentity(ctx,
                                                       IdentityType.SamAccountName,
                                                       username);

            if (usr != null)
            {
                if (Convert.ToBoolean(usr.Enabled))
                {
                    ConfigVar.IsEnable = Convert.ToBoolean(usr.Enabled);
                    Console.WriteLine("User is enable");
                    if (usr.SamAccountName != null)
                    {
                        Console.WriteLine("User samAccountName:  " + usr.SamAccountName);
                        ConfigVar.Username = usr.SamAccountName;
                    }
                    if (usr.EmailAddress != null)
                    {
                        Console.WriteLine("User email:  " + usr.EmailAddress);
                        ConfigVar.UserMail = usr.EmailAddress;
                    }
                    if (usr.VoiceTelephoneNumber != null)
                    {
                        Console.WriteLine("User voiceTelNumber:  " + usr.VoiceTelephoneNumber);
                        ConfigVar.UserPhon = usr.VoiceTelephoneNumber;
                    }
                    if (usr.GivenName != null)
                    {
                        Console.WriteLine("User GivenName:  " + usr.GivenName);
                        ConfigVar.UserFnam = usr.GivenName;
                    }
                    if (usr.Surname != null)
                    {
                        Console.WriteLine("User Surname:  " + usr.Surname);
                        ConfigVar.UserLnam = usr.Surname;
                    }

                    Console.WriteLine("User displayname  " + usr.DisplayName);
                    Console.WriteLine("User name  " + usr.Name);
                }
                else
                {
                    ConfigVar.IsEnable = Convert.ToBoolean(usr.Enabled);
                    Console.WriteLine("User " + usr.SamAccountName + " is disable in active directory");
                }
            }
            else
            {
                Console.WriteLine("User is Null");
            }
            ctx.Dispose();
        }


        public static bool MemberOfGroup(string username, string groupname)
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, ConfigVar.LdapServerHost);

            UserPrincipal usr = UserPrincipal.FindByIdentity(ctx,
                                                       IdentityType.SamAccountName,
                                                       username);

            GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx,
                                                       IdentityType.SamAccountName, groupname);

            if (usr != null)
            {
                if (group != null)
                {
                    if (usr.IsMemberOf(group))
                    {
                        Console.WriteLine("User '" + username + "' is part of the group: " + groupname);
                        ctx.Dispose();
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("User '" + username + "' is NOT part of the group: " + groupname);
                        ctx.Dispose();
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("Group '" + groupname + "' Not Found:   ");
                    ctx.Dispose();
                    return false;
                }
            }
            else
            {
                Console.WriteLine("User '" + username + "' Not Found!");
                ctx.Dispose();
                return false;
            }
        }
    }
}
