﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using ConnLDAPForm.libraries;
using ConnLDAPForm.utilities;

namespace ConnLDAPForm.utilities
{
    class InstallUtil
    {


        static void GetAccessLevel()
        {

            DatabaseUtil connection = new DatabaseUtil();
            connection.OpenConection();
            connection.GetSecurityValues();
            connection.CloseConnection();

        }

        static void SetJSONSites()
        {
            JObject Sites = new JObject();

            DatabaseUtil connection = new DatabaseUtil();
            connection.OpenConection();
            connection.GetSites();
            connection.CloseConnection();

            using (StreamWriter file = File.CreateText(@"c:\Sites.json"))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                foreach (string site in ConfigVar.Sites)
                {
                    Sites.Add(site, "");
                }
                Sites.WriteTo(writer);
            }
        }

    }
}
