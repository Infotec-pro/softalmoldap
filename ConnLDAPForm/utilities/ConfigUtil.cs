﻿using ConnLDAPForm.libraries;
using System;
using System.Configuration;
using IniParser;
using IniParser.Model;
using System.Data.Odbc;
using CsvHelper;
using System.IO;

namespace ConnLDAPForm.utilities
{
    class ConfigUtil
    {
        public static void GetConfig()
        {
            GetConnectionStrings();
            GetAppSettings();
            ReadSoftalmoIni();
        }

        public static void GetConnectionStrings()
        {
            ConnectionStringSettingsCollection settings =
                ConfigurationManager.ConnectionStrings;

            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    switch (cs.Name)
                    {
                        case "LdapServer":
                            {
                                ConfigVar.LdapServerHost = cs.ConnectionString;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Unknow parameter name in App.config, ConnectionString Section : " + cs.Name);
                                break;
                            }
                    }
                }
            }
        }

        public static void GetAppSettings()
        {
            foreach (string key in ConfigurationManager.AppSettings)
            {
                switch (key)
                {
                    case "Debug":
                        {
                            ConfigVar.Debug = Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
                            break;
                        }
                    case "Mode":
                        {
                            ConfigVar.Debug = Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
                            break;
                        }
                    case "SoftalmoGroupName":
                        {
                            ConfigVar.Debug = Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
                            break;
                        }
                    case "UpdateUser":
                        {
                            ConfigVar.UpdateUser = Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
                            break;
                        }
                    case "SecureLDAP":
                        {
                            ConfigVar.SecureLDAP = Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
                            break;
                        }
                    case "LdapSSLPort":
                        {
                            ConfigVar.LdapServerPort = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "DatabaseConnection":
                        {
                            ConfigVar.DatabaseConnection = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "SoftalmoPath":
                        {
                            ConfigVar.SoftalmoPath = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "UserFilePath":
                        {
                            ConfigVar.UserFilePath = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "UserFileType":
                        {
                            ConfigVar.UserFileType = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Unknow parameter name in App.config AppSettings Section : " + key);
                            break;
                        }
                }
            }
            ReadSoftalmoIni();
            DirectoryUtil.getConnectionString();
            ConfigVar.UseConnectedUser = false;
        }

        public static void ReadUserCSVFile(string username)
        {
            /*
            TextReader UsersReader = new StreamReader("C:\\csharp.net-informations.txt");
            var csv = new CsvReader(UsersReader);
            var records = csv.GetRecord()
            */
            string filepath = @"" + ConfigVar.UserFilePath;

            string strConn = @"Driver={Microsoft Text Driver (*.txt; *.csv)};" +
            "Dbq=C:;Extensions=csv,txt";


            OdbcConnection objCSV = new OdbcConnection(strConn);
            objCSV.Open();

            OdbcCommand oCmd = new OdbcCommand("select * " +
              "from "+ filepath +" where SamAccountName='"+username+"'", objCSV);
            OdbcDataReader oDR = oCmd.ExecuteReader();

            while (oDR.Read())
            {
                Console.WriteLine(oDR.GetInt32(0) + ", " + oDR.GetString(1));
            }
        }

        public static void ReadSoftalmoIni()
        {
            Console.WriteLine("Reading Softalmo.ini file");
            Console.WriteLine("ini path: " + ConfigVar.SoftalmoPath);
            Console.WriteLine("Connection in config file: " + ConfigVar.DatabaseConnection);
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile(ConfigVar.SoftalmoPath + "Softalmo.ini");
            ConfigVar.SqlServerINI = data[ConfigVar.DatabaseConnection]["Server"];
            ConfigVar.SqlDatabaseINI = data[ConfigVar.DatabaseConnection]["Database"];
            Console.WriteLine("SQL Server: " + ConfigVar.SqlServerINI);
            Console.WriteLine("SQL Database: " + ConfigVar.SqlDatabaseINI);
        }

    }
}
