﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using ConnLDAPForm.libraries;
using ConnLDAPForm.utilities;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;

namespace ConnLDAPForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBoxSites.DataSource = ConfigVar.Sites;
            ConfigVar.DefaultSite = comboBoxSites.Text;
            checkBoxUseConnectedUser.Checked = true;
            ConfigVar.UseConnectedUser = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ConfigVar.UseConnectedUser = checkBoxUseConnectedUser.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(ConfigVar.UseConnectedUser)
            {
                ProcessUser(true);
            }
            else
            {
                ConfigVar.LdapUser = textBoxUsername.Text;
                ConfigVar.LdapPassword = textBoxPassword.Text;
                if(ConfigVar.LdapUser != "" && ConfigVar.LdapPassword != "")
                {
                    if(DirectoryUtil.CheckUserCredentials())
                    {
                        ProcessUser(false);
                    }
                    else
                    {
                        MessageBox.Show("Utilisateur ou mot de passe incorrect...", "Avertissement",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez fournir un nom d'utilisateur, mot de passe...", "Avertissement",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        static void CheckPermissions(string username)
        {
            string[] grouplist = ConfigurationManager.AppSettings["GroupList"].ToString().Split(';');
            List<string> Perms = new List<string>();

            foreach (string group in grouplist)
            {
                if (DirectoryUtil.MemberOfGroup(username, group))
                {
                    Console.WriteLine("User is in group : " + group);
                    // apply group to array of permission for softalmo (thus you check the group here, only if not found in softalmo db
                    JsonTextReader reader = new JsonTextReader(new StringReader(File.ReadAllText(@"libraries\Profiles.json")));
                    bool matched = false;
                    while (reader.Read())
                    {
                        if (reader.Value != null)
                        {
                            if (matched)
                            {
                                if (reader.TokenType.ToString() == "String" && reader.Value.ToString() != "")
                                {
                                    Console.WriteLine("AD group  Ophtalmo matched with  " + reader.Value.ToString());
                                    Perms.Add(reader.Value.ToString());
                                    matched = false;
                                }
                            }
                            else if (reader.TokenType.ToString() == "PropertyName" && reader.Value.ToString() == group)
                            {
                                Console.WriteLine("Matched on property: " + reader.Value.ToString());
                                matched = true;
                            }

                        }
                        else
                        {
                            Console.WriteLine("Token: {0}", reader.TokenType);
                        }
                    }

                }
                else
                {
                    Console.WriteLine("User is not in group : " + group);
                }
            }
            ConfigVar.Permissions = Perms;
        }

        static void ProcessUser(bool useLogged)
        {
            string loggedUser;
            if (useLogged)
            {
                loggedUser = DirectoryUtil.GetMachineUsername();
            }
            else
            {
                loggedUser = ConfigVar.LdapUser;
            }

            DatabaseUtil connection = new DatabaseUtil();
            
            // Getting user information from Active Directory Services
            DirectoryUtil.GetUser(loggedUser);

            if(ConfigVar.IsEnable)
            {
                connection.OpenConection();

                string data = connection.SelectUser(loggedUser);
                connection.SelectSite(ConfigVar.DefaultSite);

                switch (data)
                {
                    case "isdeleted":
                        {
                            MessageBox.Show("Votre compte est désactivé dans Softalmo...", "Erreur",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }
                    case "tocreate":
                        {
                            CheckPermissions(loggedUser);
                            Console.WriteLine("Need to create account: " + loggedUser);
                            connection.InsertUser(loggedUser);
                            var dataCreated = connection.SelectUser(loggedUser);
                            Console.WriteLine("Starting Softalmo with account: " + loggedUser);
                            StartSoftalmo(loggedUser, dataCreated);
                            break;
                        }
                    case "":
                        {
                            MessageBox.Show("Mot de passe vide dans la base de données...", "Erreur",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }
                    case null:
                        {
                            MessageBox.Show("Mot de passe null dans la base de données...", "Erreur",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Starting Softalmo with account: "+loggedUser);
                            StartSoftalmo(loggedUser, data);
                            break;
                        }
                }
                connection.CloseConnection();
            }
            else
            {
                MessageBox.Show("Votre compte est désactivé, veuillez contacter votre Administrateur...", "Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (ConfigVar.Debug)
            {
                Console.WriteLine("Veuillez appuyer sur une touche pour terminer...");
                Console.ReadLine();
            }
        }


        static void StartSoftalmo(string username, string password)
        {
            String appFile = @"" + ConfigVar.SoftalmoPath + "Softalmo.Commander.Net.exe";
            String args = "-username \"" + username + "\" -password \"" + password + "\" -cn \"" + ConfigVar.DatabaseConnection + "\" -command \"activate\" -launch -timeout 100000";
            
            if (File.Exists(appFile))
            {
                Console.WriteLine("Starting Softalmo Commander with user: " + username);
                Process.Start(appFile, args);
                Application.Exit();
            }
            else
            {
                Console.WriteLine("Cannot find Softalmo.Commander.Net.exe, check config file for SoftalmoPath: ");
                Console.WriteLine(ConfigVar.SoftalmoPath);
                MessageBox.Show("Impossible de trouver l'executable Softalmo Commander, verifiez le chemin d'accès", "Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comboBoxSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigVar.DefaultSite = comboBoxSites.Text;
            Console.WriteLine("Site selected changed to: " + ConfigVar.DefaultSite);
        }
        
        private void textBoxUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(this, new EventArgs());
            }
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(this, new EventArgs());
            }
        }
        
    }
}
