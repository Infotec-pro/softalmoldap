﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnLDAPForm.libraries
{
    class ConfigVar
    {
        /// <summary>
        /// Global variables.
        /// </summary>

        static bool _updateUser;
        static bool _debug;
        static bool _secureLDAP;
        static bool _useConnectedUser;

        static string _newAccessValue;
        static string _editAccessValue;
        static string _deleteAccessValue;

        static string _ldapServerHost;
        static string _ldapServerPort;
        static string _userFileType;
        static string _userFilePath;


        static string _softalmoPath;
        static string _databaseConnection;
        static string _sqlServerINI;
        static string _sqlDatabaseINI;
        static string _defaultSite;
        static string _selectedSite;

        static string _groupList;
        static List<string> _permissions;
        static List<string> _sites;

        static string _ldapUser;
        static string _ldapPassword;

        static string _username;
        static string _userfnam;
        static string _userlnam;
        static string _usermail;
        static string _userphon;
        static bool _isEnable;


        

        public static bool UpdateUser
        {
            get
            {
                return _updateUser;
            }
            set
            {
                _updateUser = value;
            }
        }

        public static bool Debug
        {
            get
            {
                return _debug;
            }
            set
            {
                _debug = value;
            }
        }

        public static bool SecureLDAP
        {
            get
            {
                return _secureLDAP;
            }
            set
            {
                _secureLDAP = value;
            }
        }


        public static bool UseConnectedUser
        {
            get
            {
                return _useConnectedUser;
            }
            set
            {
                _useConnectedUser = value;
            }
        }

        public static string NewAccessValue
        {
            get
            {
                return _newAccessValue;
            }
            set
            {
                _newAccessValue = value;
            }
        }


        public static string EditAccessValue
        {
            get
            {
                return _editAccessValue;
            }
            set
            {
                _editAccessValue = value;
            }
        }

        public static string DeleteAccessValue
        {
            get
            {
                return _deleteAccessValue;
            }
            set
            {
                _deleteAccessValue = value;
            }
        }



        public static string SqlServerINI
        {
            get
            {
                return _sqlServerINI;
            }
            set
            {
                _sqlServerINI = value;
            }
        }
        public static string SqlDatabaseINI
        {
            get
            {
                return _sqlDatabaseINI;
            }
            set
            {
                _sqlDatabaseINI = value;
            }
        }
        public static string GroupList
        {
            get
            {
                return _groupList;
            }
            set
            {
                _groupList = value;
            }
        }

        public static List<string> Sites
        {
            get
            {
                return _sites;
            }
            set
            {
                _sites = value;
            }
        }

        public static List<string> Permissions
        {
            get
            {
                return _permissions;
            }
            set
            {
                _permissions = value;
            }
        }

        public static string LdapServerHost
        {
            get
            {
                return _ldapServerHost;
            }
            set
            {
                _ldapServerHost = value;
            }
        }

        public static string LdapServerPort
        {
            get
            {
                return _ldapServerPort;
            }
            set
            {
                _ldapServerPort = value;
            }
        }


        public static string SoftalmoPath
        {
            get
            {
                return _softalmoPath;
            }
            set
            {
                _softalmoPath = value;
            }
        }

        public static string UserFilePath
        {
            get
            {
                return _userFilePath;
            }
            set
            {
                _userFilePath = value;
            }
        }

        public static string UserFileType
        {
            get
            {
                return _userFileType;
            }
            set
            {
                _userFileType = value;
            }
        }

        public static string DatabaseConnection
        {
            get
            {
                return _databaseConnection;
            }
            set
            {
                _databaseConnection = value;
            }
        }

        public static string DefaultSite
        {
            get
            {
                return _defaultSite;
            }
            set
            {
                _defaultSite = value;
            }
        }

        public static string SelectedSite
        {
            get
            {
                return _selectedSite;
            }
            set
            {
                _selectedSite = value;
            }
        }


        public static string LdapUser
        {
            get
            {
                return _ldapUser;
            }
            set
            {
                _ldapUser = value;
            }
        }

        public static string LdapPassword
        {
            get
            {
                return _ldapPassword;
            }
            set
            {
                _ldapPassword = value;
            }
        }

        public static string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        public static string UserMail
        {
            get
            {
                return _usermail;
            }
            set
            {
                _usermail = value;
            }
        }

        public static string UserPhon
        {
            get
            {
                return _userphon;
            }
            set
            {
                _userphon = value;
            }
        }

        public static string UserFnam
        {
            get
            {
                return _userfnam;
            }
            set
            {
                _userfnam = value;
            }
        }

        public static string UserLnam
        {
            get
            {
                return _userlnam;
            }
            set
            {
                _userlnam = value;
            }
        }

        public static bool IsEnable
        {
            get
            {
                return _isEnable;
            }
            set
            {
                _isEnable = value;
            }
        }
    }
}
