﻿using System;
using System.Windows.Forms;
using ConnLDAPForm.libraries;
using ConnLDAPForm.utilities;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics.CodeAnalysis;

namespace ConnLDAPForm
{
    static class Program
    {

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern int AllocConsole();

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern int FreeConsole();

        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*
             *  Debug console (on/off)
             */
            const int SW_HIDE = 0;
            const int SW_SHOW = 5;
            
            var handle = GetConsoleWindow();
            
            // Reading config file for <connectionStrings> & <AppSettings>
            ConfigUtil.GetConnectionStrings();
            ConfigUtil.GetAppSettings();
            ConfigUtil.ReadUserCSVFile("nbersez");

            Console.ReadLine();

            DatabaseUtil connection = new DatabaseUtil();
            connection.OpenConection();
            connection.GetSites();
            connection.GetSecurityValues();
            connection.CloseConnection();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (ConfigVar.Debug)
            {
                ShowWindow(handle, SW_SHOW);
            }
            else
            {
                ShowWindow(handle, SW_HIDE);
            }

            AllocConsole();

            Application.Run(new Form1());

            FreeConsole();
        }
    }
}
