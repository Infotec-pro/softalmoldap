﻿using System;
using IniParser;
using IniParser.Model;

namespace SoftalmoLDAP

{
    class INIReader
    {
        

        public static void ReadSoftalmoIni()
        {
            Console.WriteLine("Reading Softalmo.ini file");
            Console.WriteLine("ini path: " + ConfigVar.SoftalmoPath);
            Console.WriteLine("Connection in config file: " + ConfigVar.DatabaseConnection);
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile(ConfigVar.SoftalmoPath+"Softalmo.ini");
            ConfigVar.SqlServerINI = data[ConfigVar.DatabaseConnection]["Server"];
            ConfigVar.SqlDatabaseINI = data[ConfigVar.DatabaseConnection]["Database"];
            Console.WriteLine("SQL Server: " + ConfigVar.SqlServerINI);
            Console.WriteLine("SQL Database: " + ConfigVar.SqlDatabaseINI);
        }

    }
}
