﻿using System.Collections.Generic;

namespace SoftalmoLDAP
{
    class ConfigVar
    {
        /// <summary>
        /// Global variables.
        /// </summary>

        static bool _debug;
        static bool _isEnable;
        static bool _secureLdap;
        static bool _useConnectedUser;

        static int _ldapServerPort;

        static string _ldapServer;
        static string _userFileType;
        static string _userFilePath;
        static string _groupList;

        static string _softalmoPath;
        static string _databaseConnection;
        static string _sqlServerINI;
        static string _sqlDatabaseINI;
        static string _defaultSite;
        
        static string _username;
        static string _userfnam;
        static string _userlnam;
        static string _usermail;
        static string _userphon;
        
        static List<string> _permissions;

        public static bool Debug
        {
            get
            {
                return _debug;
            }
            set
            {
                _debug = value;
            }
        }

        public static bool IsEnable
        {
            get
            {
                return _isEnable;
            }
            set
            {
                _isEnable = value;
            }
        }

        public static bool SecureLdap
        {
            get
            {
                return _secureLdap;
            }
            set
            {
                _secureLdap = value;
            }
        }

        public static bool UseConnectedUser
        {
            get
            {
                return _useConnectedUser;
            }
            set
            {
                _useConnectedUser = value;
            }
        }



        public static int LdapServerPort
        {
            get
            {
                return _ldapServerPort;
            }
            set
            {
                _ldapServerPort = value;
            }
        }

        public static string UserFileType
        {
            get
            {
                return _userFileType;
            }
            set
            {
                _userFileType = value;
            }
        }

        public static string UserFilePath
        {
            get
            {
                return _userFilePath;
            }
            set
            {
                _userFilePath = value;
            }
        }



        public static string SqlServerINI
        {
            get
            {
                return _sqlServerINI;
            }
            set
            {
                _sqlServerINI = value;
            }
        }

        public static string SqlDatabaseINI
        {
            get
            {
                return _sqlDatabaseINI;
            }
            set
            {
                _sqlDatabaseINI = value;
            }
        }
        public static string GroupList
        {
            get
            {
                return _groupList;
            }
            set
            {
                _groupList = value;
            }
        }
        public static List<string> Permissions
        {
            get
            {
                return _permissions;
            }
            set
            {
                _permissions = value;
            }
        }
        
        public static string LdapServer
        {
            get
            {
                return _ldapServer;
            }
            set
            {
                _ldapServer = value;
            }
        }

        public static string SoftalmoPath
        {
            get
            {
                return _softalmoPath;
            }
            set
            {
                _softalmoPath = value;
            }
        }

        public static string DatabaseConnection
        {
            get
            {
                return _databaseConnection;
            }
            set
            {
                _databaseConnection = value;
            }
        }

        public static string DefaultSite
        {
            get
            {
                return _defaultSite;
            }
            set
            {
                _defaultSite = value;
            }
        }
        
        public static string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        public static string UserMail
        {
            get
            {
                return _usermail;
            }
            set
            {
                _usermail = value;
            }
        }

        public static string UserPhon
        {
            get
            {
                return _userphon;
            }
            set
            {
                _userphon = value;
            }
        }

        public static string UserFnam
        {
            get
            {
                return _userfnam;
            }
            set
            {
                _userfnam = value;
            }
        }

        public static string UserLnam
        {
            get
            {
                return _userlnam;
            }
            set
            {
                _userlnam = value;
            }
        }

    }
}
