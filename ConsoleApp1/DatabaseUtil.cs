﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SoftalmoLDAP
{
    class DatabaseUtil
    {
        string ConnectionString = "Server=" + ConfigVar.SqlServerINI + ";Database="+ ConfigVar.SqlDatabaseINI + ";User Id=SoftalmoUser;Password=freak;";
        SqlConnection con;

        public void TagUserAsDeleted(string username)
        {
            ExecuteQueries(
                "UPDATE SoftalmoUser.user_ SET isdeleted=1 WHERE cuserid='LDAP_" + username + "';"
                );
        }

        public void InsertUser(string username)
        {
            ExecuteQueries(
                "INSERT INTO SoftalmoUser.user_ (cuserid, username, naam, vnaam, titel, firstnamealpha, lastnamealpha, paswoord, csecurity, ctypeid,cccowid,cmiddlename,cprefix,adres,stad,pc,tel1,cemail,corganizationname,rizivnumm,banknumm,lprotected,lordered,lagreed,nfisc,ldefault,lsystem,isdeleted,pwlocked) VALUES('LDAP_"+ username + "','" +
                ""+ username + "', '" + ConfigVar.UserLnam + "', '" + ConfigVar.UserFnam + "', 'Docteur', '" + ConfigVar.UserFnam + "', '" + ConfigVar.UserLnam + "', '" + username + "', '99', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', '" + ConfigVar.UserPhon + "', '" + ConfigVar.UserMail +"', 'NULL', 'NULL', 'NULL', 0, 0, 0, 0, 0, 0, 0, 0);"
                );
            foreach (string perm in ConfigVar.Permissions)
            {
                Console.WriteLine("permission of user: "+perm);
                ExecuteQueries(
                "INSERT INTO SoftalmoUser.usrroles (usrroleid, userid, roleid) VALUES('LDAP_" + username + "_" + perm.Trim() + "', 'LDAP_"+ username +"', '"+ perm +"');"
                );
            }
            ExecuteQueries(
                "INSERT INTO SoftalmoUser.usrgrp (cusrgrpid, cuserid, cgrpid) VALUES('LDAP_" + username +"_"+ConfigVar.DefaultSite+"', 'LDAP_" + username + "', '"+ConfigVar.DefaultSite+"');"
                );
        }

        public string SelectUser(string username)
        {
            SqlCommand cmd = new SqlCommand("SELECT * from SoftalmoUser.user_ WHERE username='"+ username +"';", con);
            
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Console.WriteLine("USER FOUND:  " + dr["username"]);
                        if(dr["isdeleted"].Equals(0))
                        {
                            Console.WriteLine("USER IS ACTIVE! isdeleted="+ dr["isdeleted"].ToString());

                            if (ConfigVar.IsEnable)
                            {
                                return dr["paswoord"].ToString();
                            }
                            else
                            {
                                TagUserAsDeleted(username);
                                Console.WriteLine("Your AD account is not enable.");
                                return "isdeleted";
                            }

                        }
                    }
                    Console.WriteLine("USER IS DELETED! isdeleted=" + dr["isdeleted"].ToString());
                    return "isdeleted";
                }
                else
                {
                    Console.WriteLine("USER NOT FOUND!");
                    return "tocreate";
                }
            }
        }
        public void SelectSite(string cname)
        {
            SqlCommand cmd = new SqlCommand("SELECT * from SoftalmoUser.usergroups WHERE cname='" + cname + "';", con);

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Console.WriteLine("SITE FOUND:  " + dr["cgrpid"] + " cname: "+dr["cname"]);
                        ConfigVar.DefaultSite = dr["cgrpid"].ToString();
                    }
                }
                else
                {
                    Console.WriteLine("SITE NOT FOUND: "+cname);
                }
            }
        }
        public void UpdatePermissions(string username)
        {
            foreach (string perm in ConfigVar.Permissions)
            {
                Console.WriteLine("permission of user: " + perm);
                ExecuteQueries(
                "UPDATE INTO SoftalmoUser.usrroles (usrroleid, userid, roleid) VALUES('LDAP_" + username + "_" + perm.Trim() + "', 'LDAP_" + username + "', '" + perm + "');"
                );
            }
        }

        public void OpenConection()
        {
            con = new SqlConnection(ConnectionString);
            con.Open();
        }

        public void CloseConnection()
        {
            con.Close();
        }

        public void ExecuteQueries(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.ExecuteNonQuery();
        }

        public SqlDataReader DataReader(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public object ShowDataInGridView(string Query_)
        {
            SqlDataAdapter dr = new SqlDataAdapter(Query_, ConnectionString);
            DataSet ds = new DataSet();
            dr.Fill(ds);
            object dataum = ds.Tables[0];
            return dataum;
        }
    }
}
