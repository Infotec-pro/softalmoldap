﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace SoftalmoLDAP
{
    class Program
    {

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static void Main(string[] args)
        {

            // Reading config file for <connectionStrings> & <AppSettings>
            GetConnectionStrings();
            GetAppSettings();

            /*
             * 
             *  Debug console (on/off)
             * 
             */
            const int SW_HIDE = 0;
            const int SW_SHOW = 5;

            var handle = GetConsoleWindow();
            if (ConfigVar.Debug)
            {
                ShowWindow(handle, SW_SHOW);
            }
            else
            {
                ShowWindow(handle, SW_HIDE);
            }

            DatabaseUtil connection = new DatabaseUtil();

            string loggedUser = LdapUtil.GetMachineUsername();


            // Getting user information from Active Directory Services
            LdapUtil.GetUser(loggedUser);

            
            connection.OpenConection();
            
            string data = connection.SelectUser(loggedUser);
            connection.SelectSite(ConfigVar.DefaultSite);

            switch (data)
            {
                case "isdeleted":
                    {
                        Console.WriteLine("Account '" + loggedUser + "' is tagged as deleted in softalmo...");
                        break;
                    }
                case "tocreate":
                    {
                        CheckPermissions();
                        Console.WriteLine("Need to create account: " + loggedUser);
                        connection.InsertUser(loggedUser);
                        var dataCreated = connection.SelectUser(loggedUser);
                        StartSoftalmo(loggedUser, dataCreated);
                        break;
                    }
                case "":
                    {
                        Console.WriteLine("Empty paswoord field...");
                        break;
                    }
                case null:
                    {
                        Console.WriteLine("paswoord is null...");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Starting Softalmo with account");
                        StartSoftalmo(loggedUser, data);
                        break;
                    }
            }

            connection.CloseConnection();

            if (ConfigVar.Debug)
            {
                Console.WriteLine("Veuillez appuyer sur une touche pour terminer...");
                Console.ReadLine();
            }
        }


        static void GetConnectionStrings()
        {
            ConnectionStringSettingsCollection settings =
                ConfigurationManager.ConnectionStrings;

            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    switch (cs.Name)
                    {
                        case "ADConnectionString":
                            {
                                ConfigVar.LdapServer = cs.ConnectionString;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Unknow parameter name in App.config, ConnectionString Section : " + cs.Name);
                                break;
                            }
                    }
                }
            }
        }

        static void GetAppSettings()
        {
            foreach (string key in ConfigurationManager.AppSettings)
            {
                switch (key)
                {
                    case "Debug":
                        {
                            ConfigVar.Debug = Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
                            break;
                        }
                    case "DatabaseConnection":
                        {
                            ConfigVar.DatabaseConnection = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "SoftalmoPath":
                        {
                            ConfigVar.SoftalmoPath = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "GroupList":
                        {
                            ConfigVar.GroupList = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    case "DefaultSite":
                        {
                            ConfigVar.DefaultSite = ConfigurationManager.AppSettings[key];
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Unknow parameter name in App.config AppSettings Section : " + key);
                            break;
                        }
                }
            }
            INIReader.ReadSoftalmoIni();
        }

        static void StartSoftalmo(string username, string password)
        {
            Console.WriteLine("Starting Softalmo Commander with user: "+username);
            String appFile = @"" + ConfigVar.SoftalmoPath + "Softalmo.Commander.Net.exe";

            String args = "-username \"" + username + "\" -password \"" + password + "\" -cn \"" + ConfigVar.DatabaseConnection + "\" -command \"activate\" -launch -timeout 100000";

            Process.Start(appFile, args);
        }

        static void CheckPermissions()
        {
            string[] grouplist = ConfigurationManager.AppSettings["GroupList"].ToString().Split(';');
            string username = LdapUtil.GetMachineUsername();
            List<string> Perms = new List<string>();

            foreach (string group in grouplist)
            {
                if (LdapUtil.MemberOfGroup(username, group))
                {
                    Console.WriteLine("User is in group : " + group);
                    // apply group to array of permission for softalmo (thus you check the group here, only if not found in softalmo db
                    JsonTextReader reader = new JsonTextReader(new StringReader(File.ReadAllText(@"Profiles.json")));
                    bool matched = false;
                    while (reader.Read())
                    {
                        if (reader.Value != null)
                        {
                            if (matched)
                            {
                                if (reader.TokenType.ToString() == "String" && reader.Value.ToString() != "")
                                {
                                    Console.WriteLine("AD group  Ophtalmo matched with  " + reader.Value.ToString());
                                    Perms.Add(reader.Value.ToString());
                                    matched = false;
                                }
                            }
                            else if (reader.TokenType.ToString() == "PropertyName" && reader.Value.ToString() == group)
                            {
                                Console.WriteLine("Matched on property: " + reader.Value.ToString());
                                matched = true;
                            }

                        }
                        else
                        {
                            Console.WriteLine("Token: {0}", reader.TokenType);
                        }
                    }

                }
                else
                {
                    Console.WriteLine("User is not in group : " + group);
                }
            }
            ConfigVar.Permissions = Perms;
        }
    }
}
